﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        // GET: Categorie
        public ActionResult Index(int page = 1, string searchValue = "")
        {
            int rowCount = 0;
            int pageSize = 5;
            var listOfCategorys = DataService.ListCategory(page, pageSize, searchValue, out rowCount);

            var model = new Models.CategoryPaginationQueryResult()
            {
                Page = page,
                PageSize = pageSize,
                SearchValue = searchValue,
                RowCount = rowCount,
                Data = listOfCategorys
            };
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Thay Đổi Thông Tin Loại Hàng";
            var model = DataService.GetCategory(id);
            if (model == null)
                RedirectToAction("Index");

            return View(model);
        }
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ Sung Thông Tin Loại Hàng";
            Category model = new Category()
            {
                CategoryID = 0
            };
            return View("Edit", model);
        }
        public ActionResult Delete(int id)
        {
            if (Request.HttpMethod == "POST")
            {
                //xóa supplier có mã là id
                DataService.DeleteCategory(id);

                //Quay về lại trang index
                return RedirectToAction("Index");

            }
            else
            {
                //lấy thông tin của category cần xóa
                var model = DataService.GetCategory(id);
                if (model == null)
                    return RedirectToAction("Index");

                //trả thông tin về cho view để hiển thị
                return View(model);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult Save(Category data)
        {
            try
            {
                //return Json(data);
                if (string.IsNullOrWhiteSpace(data.CategoryName))
                    ModelState.AddModelError("CategoryName", "Vui lòng nhập tên của loại hàng");
                if (string.IsNullOrEmpty(data.Description))
                    data.Description = ""; 

                if (!ModelState.IsValid)
                {
                    if (data.CategoryID == 0)
                        ViewBag.Title = "Bổ sung loại hàng mơi";
                    else
                        ViewBag.Title = "Thay đổi thông tin loại hàng";
                    return View("Edit", data);
                }



                if (data.CategoryID== 0)
                    DataService.AddCategory(data);
                else
                    DataService.UpdateCategory(data.CategoryID,data);

                return RedirectToAction("Index");
            }
            catch
            {
                return Content("Oops! Trang nay khong ton tai :)");
            }
        }
    }
}