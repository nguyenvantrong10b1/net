﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LiteCommerce.DataLayers;
using LiteCommerce.DataLayers.SQLServer;
using LiteCommerce.DomainModels;
namespace LiteCommerce.Admin.Controllers
{
    public class TestController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        // GET: Test
        public ActionResult Index()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LiteCommerceDB"].ConnectionString;
            //ICountryDAL dal = new DataLayers.SQLServer.CountryDAL(connectionString);
            //var data = dal.List();
            //ICityDAL dal = new DataLayers.SQLServer.CityDAL(connectionString);
            //var data = dal.List();
            //ISupplierDAL dal = new SupplierDAL(connectionString);
            //var data = dal.Count("");
            //ISupplierDAL dal = new SupplierDAL(connectionString);
            //var data = dal.Get(1);

            //DateTimeConverter c = new DateTimeConverter();
            //  BirthDate = (DateTime)c.ConvertFromString("04/03/2000")

            IShipperDAL dal = new ShipperDAL(connectionString);
            Shipper s = new Shipper()
            {
                ShipperName = "nguyen van trong",
                Phone= "8888",

            };
            var data = dal.Add(s);//Update

            //ISupplierDAL dal = new SupplierDAL(connectionString);
            //Supplier s = new Supplier();
            //var data = dal.Delete(30);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Pagination(int page, int pageSize, string searchvalue)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LiteCommerceDB"].ConnectionString;
            //ICountryDAL dal = new DataLayers.SQLServer.CountryDAL(connectionString);
            //var data = dal.List();
            //ISupplierDAL dal = new DataLayers.SQLServer.SupplierDAL(connectionString);
            //var data = dal.List(page, pageSize, searchvalue);
            //ICategoryDAL dal = new DataLayers.SQLServer.CategoryDAL(connectionString);
            //var data = dal.List(page, pageSize, searchvalue);
            //IShipperDAL dal = new DataLayers.SQLServer.ShipperDAL(connectionString);
            //var data = dal.List(page, pageSize, searchvalue);
            IEmployeeDAL dal = new DataLayers.SQLServer.EmployeeDAL(connectionString);
            var data = dal.List(page, pageSize, searchvalue);



            return Json(data, JsonRequestBehavior.AllowGet);
        }
    
        //http://localhost:57527/Test/Pagination?page=2&pageSize=5&searchValue=
    }
}