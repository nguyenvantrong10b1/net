﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index(int page = 1, string searchValue = "")
        {
            //var model = HRService.Employee_List();
            int rowCount = 0;
            int pageSize = 5;
            var listOfEmployees = DataService.ListEmployee(page, pageSize, searchValue, out rowCount);

            var model = new Models.EmployeePaginationQueryResult()
            {
                Page = page,
                PageSize = pageSize,
                SearchValue = searchValue,
                RowCount = rowCount,
                Data = listOfEmployees
            };
            return View(model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Thay đổi thông tin nhân viên";
          

            var model = DataService.GetEmployee(id);
            if (model == null)
                RedirectToAction("Index");

            return View(model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            ViewBag.Title = "Thêm thông tin nhân viên";
            
            Employee model = new Employee()
            {
                EmployeeID = 0
            };
            return View("Edit", model);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            if (Request.HttpMethod == "POST")
            {
                //xóa employee có mã là id
                DataService.DeleteEmployee(id);

                //Quay về lại trang index
                return RedirectToAction("Index");

            }
            else
            {
                //lấy thông tin của employee cần xóa
                var model = DataService.GetEmployee(id);
                if (model == null)
                    return RedirectToAction("Index");

                //trả thông tin về cho view để hiển thị
                return View(model);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Save(Employee data)
        {
            try
            {
                //return Json(data);
                if (string.IsNullOrWhiteSpace(data.LastName))
                    ModelState.AddModelError("LastName", "Vui lòng nhập tên của nhân viên");
                if (string.IsNullOrWhiteSpace(data.FirstName))
                    ModelState.AddModelError("FirstName", "Bạn chưa nhập tên nhân viên");
                if (string.IsNullOrEmpty(Convert.ToString(data.BirthDate)))
                    ModelState.AddModelError("BirthDate", "Bạn chưa nhập ngày sinh !");
                if (string.IsNullOrEmpty(data.Photo))
                    data.Photo = "";
                if (string.IsNullOrEmpty(data.Notes))
                    data.Notes = "";
                if (string.IsNullOrEmpty(data.Email))
                    data.Email = "";
                if (string.IsNullOrEmpty(data.Password))
                    data.Password = "";

                if (!ModelState.IsValid)
                {
                    if (data.EmployeeID == 0)
                        ViewBag.Title = "Bổ sung nhân viên mơi";
                    else
                        ViewBag.Title = "Thay đổi thông tin nhân viên";
                    return View("Edit", data);
                }



                if (data.EmployeeID == 0)
                    DataService.AddEmployee(data);
                else
                    DataService.UpdateEmployee(data.EmployeeID,data);

                return RedirectToAction("Index");
            }
            catch
            {
                return Content("Oops! Trang nay khong ton tai :)");
            }
        }
    }
}