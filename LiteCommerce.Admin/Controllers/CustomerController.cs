﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index(int page = 1, string searchValue = "")
        {
            int rowCount = 0;
            int pageSize = 10;
            var listOfCustomers = DataService.ListCustomer(page, pageSize, searchValue, out rowCount);

            var model = new Models.CustomerPaginationQueryResult()
            {
                Page = page,
                PageSize = pageSize,
                SearchValue = searchValue,
                RowCount = rowCount,
                Data = listOfCustomers
            };
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Thay Đổi Thông Tin Khách Hàng";
            var model = DataService.GetCustomer(id);
            if (model == null)
                RedirectToAction("Index");

            return View(model);
        }
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ Sung Thông Tin Khách Hàng";
            Customer model = new Customer()
            {
                CustomerID = 0
            };
            return View("Edit", model);
        }
        public ActionResult Delete(int id)
        {
            if (Request.HttpMethod == "POST")
            {
                //xóa supplier có mã là id
                DataService.DeleteCustomer(id);

                //Quay về lại trang index
                return RedirectToAction("Index");

            }
            else
            {
                //lấy thông tin của supplier cần xóa
                var model = DataService.GetCustomer(id);
                if (model == null)
                    return RedirectToAction("Index");

                //trả thông tin về cho view để hiển thị
                return View(model);
            }
        }
        public ActionResult Save(Customer data)
        {
            try
            {
                //return Json(data);
                if (string.IsNullOrWhiteSpace(data.CustomerName))
                    ModelState.AddModelError("CustomerName", "Vui lòng nhập tên của khach hang");
                if (string.IsNullOrWhiteSpace(data.ContactName))
                    ModelState.AddModelError("ContactName", "Bạn chưa nhập tên liên hệ của khach hang");
                if (string.IsNullOrEmpty(data.Address))
                    data.Address = "";
                if (string.IsNullOrEmpty(data.Country))
                    data.Country = "";
                if (string.IsNullOrEmpty(data.City))
                    data.City = "";
                if (string.IsNullOrEmpty(data.PostalCode))
                    data.PostalCode = "";
                if (string.IsNullOrEmpty(data.Email))
                    data.Email = "";
                if (string.IsNullOrEmpty(data.Password))
                    data.Password = "";

                if (!ModelState.IsValid)
                {
                    if (data.CustomerID == 0)
                        ViewBag.Title = "Bổ sung nhà cung cấp mơi";
                    else
                        ViewBag.Title = "Thay đổi thông tin nhà cung cấp";
                    return View("Edit", data);
                }



                if (data.CustomerID == 0)
                    DataService.AddCustomer(data);
                else
                    DataService.UpdateCustomer(data.CustomerID,data);

                return RedirectToAction("Index");
            }
            catch
            {
                return Content("Oops! Trang nay khong ton tai :)");
            }
        }
    }
}