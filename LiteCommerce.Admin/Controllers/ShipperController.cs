﻿using LiteCommerce.BusinessLayers;
using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LiteCommerce.Admin.Controllers
{
    [Authorize]
    public class ShipperController : Controller
    {
        // GET: Shipper
        public ActionResult Index(int page = 1, string searchValue = "")
        {
            int rowCount = 0;
            int pageSize = 5;
            var listOfShippers = DataService.ListShipper(page, pageSize, searchValue, out rowCount);

            var model = new Models.ShipperPaginationQueryResult()
            {
                Page = page,
                PageSize = pageSize,
                SearchValue = searchValue,
                RowCount = rowCount,
                Data = listOfShippers
            };
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            ViewBag.Title = "Thay Đổi Thông Tin Nhà Vận Chuyển";

            var model = DataService.GetShipper(id);
            if (model == null)
                RedirectToAction("Index");

            return View(model);
        }
        public ActionResult Add()
        {
            ViewBag.Title = "Bổ Sung Thông Tin Nhà Vận Chuyển";

            Shipper model = new Shipper()
            {
                ShipperID = 0
            };
            return View("Edit", model);
        }
        public ActionResult Delete(int id)
        {
            if (Request.HttpMethod == "POST")
            {
                //xóa shipper có mã là id
                DataService.DeleteShipper(id);

                //Quay về lại trang index
                return RedirectToAction("Index");

            }
            else
            {
                //lấy thông tin của shipper cần xóa
                var model = DataService.GetShipper(id);
                if (model == null)
                    return RedirectToAction("Index");

                //trả thông tin về cho view để hiển thị
                return View(model);
            }
        }
        public ActionResult Save(Shipper data)
        {
            try
            {
                //return Json(data);
                if (string.IsNullOrWhiteSpace(data.ShipperName))
                    ModelState.AddModelError("ShipperName", "Vui lòng nhập tên của nhà Vận Chuyển");
                if (string.IsNullOrEmpty(data.Phone))
                    data.Phone = "";

                if (!ModelState.IsValid)
                {
                    if (data.ShipperID == 0)
                        ViewBag.Title = "Bổ sung nhà vận chuyển mới";
                    else
                        ViewBag.Title = "Thay đổi thông tin nhà vận chuyển";
                    return View("Edit", data);
                }



                if (data.ShipperID == 0)
                    DataService.AddShipper(data);
                else
                    DataService.UpdateShipper(data.ShipperID,data);

                return RedirectToAction("Index");
            }
            catch
            {
                return Content("Oops! Trang nay khong ton tai :)");
            }
        }
    }
}