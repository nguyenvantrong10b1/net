﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    /// <summary>
    /// Định nghĩa các phép xử lý liên quan đến nhà cung cấp
    /// </summary>
    public interface ISupplierDAL
    {
        /// <summary>
        /// Bổ sung một nhà cung cấp mới. Hàm trả về mã của nhà cung cấp
        /// nếu bổ sung thành công.
        /// </summary>
        /// <param name="data">Đối tượng lưu thông tin của nhà cung cấp cần bổ sung</param>
        /// <returns></returns>
        int Add(Supplier data);
        /// <summary>
        /// Lấy danh sách các nhà cung cấp (tìm kiếm. phân trang)
        /// </summary>
        /// <param name="page">Trang cần lấy dữ liệu</param>
        /// <param name="pageSize">Số dòng hiển thị trên mỗi trang</param>
        /// <param name="searchValue">Giá trị cần tìm kiếm theo SupplierName, ContactName, Address, City, PostalCode, Country, Phone (chuỗi rỗng nếu không tìm kiếm)</param>
        /// <returns></returns>
        List<Supplier> List(int page, int pageSize, string searchValue);
        /// <summary>
        /// Đếm số lượng nhà cung cấp thỏa điều kiện tìm kiếm
        /// </summary>
        /// <param name="searchValue">Giá trị cần tìm (chuỗi rỗng nếu không tìm kiếm)</param>
        /// <returns></returns>
        int Count(string searchValue);
        /// <summary>
        /// lấy thông tin của một nhà cung cấp theo mã.trong trường hợp nhà cung cấp
        /// không tồn tại, hàm trả về giá trị null.
        /// </summary>
        /// <param name="supplierID"></param>
        /// <returns></returns>
        Supplier Get(int supplierID);
        /// <summary>
        /// cập nhật thông tin của một nhà cung cấp. Hàm trả về giá trị booleam cho
        /// biết việc cấp nhật có thành công hay không?
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool Update(Supplier data);
        /// <summary>
        /// Xóa một nhà cung cấp dựa vào mã. Hàm trả về giá trị bool cho biết việc xóa 
        /// có thực hiện được hay không (Lưu ý: không được xóa nhà cung cấp nếu đag có
        /// mặt hàng tham chiếu đến nhà cung cấp.
        /// </summary>
        /// <param name="SupplierID"></param>
        /// <returns></returns>
        bool Delete(int SupplierID);
    }
 
}
