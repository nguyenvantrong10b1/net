﻿using LiteCommerce.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DataLayers
{
    public interface IAccountDAL
    {
        /// <summary>
        /// kiểm tra thông tin đăng nhâp của user(hàm trả về null nếu thông tin đăng nhập k hợp lệ
        /// </summary>
        /// <param name="loginName">Tên đaăng nhập</param>
        /// <param name="password"></param>
        /// <returns></returns>
        Account Authorize(string loginName, string password);
        /// <summary>
        /// doi mat khau
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="oldpassword"></param>
        /// <param name="newpassword"></param>
        /// <returns></returns>
        bool ChangePassword(string accountId, string oldpassword, string newpassword);
        /// <summary>
        /// lấy thông tin của mopojt tài khoản theo id
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        Account Get(string accountId);
    }
}
