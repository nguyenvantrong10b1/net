﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteCommerce.DomainModels
{
    /// <summary>
    /// Tài khoản của người sử dụng
    /// </summary>
    public class Account
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }
        //anh
        //Bài tập:
        //  Hiển thị đúng ảnh của employee đăng nhập
        //    Làm chức năng đổi mật khẩu
        public string Photo { get; set; }
    }
}
